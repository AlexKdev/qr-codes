from pyzbar.pyzbar import decode
from PIL import Image
import os
import sys

if __name__ == "__main__":

    if len(sys.argv) > 1:
        for i in range(1, len(sys.argv)):
            filename = sys.argv[i]
            if os.path.exists(filename):
                qrData = decode(Image.open(filename))
                print('{} : {}'.format(filename ,int(qrData[0].data)))
            else:
                print("File does not exist!")
    else:
        print('Incorrect arguments!')