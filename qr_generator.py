import sys
import pyqrcode
import csv
import os

if __name__ == "__main__":
    a = 0
    b = 100
    if len(sys.argv) >= 3:
        try:
            a = int(sys.argv[1])
        except:
            a = 0
        try:
            b = int(sys.argv[2])
        except:
            b = 100

    path = os.getcwd()
    qrPath = path + 'qr\\'
    if len(sys.argv) == 4:
        path = sys.argv[3]
        qrPath = path + 'qr\\'
    if not os.path.exists(qrPath):
        os.mkdir(path+'qr')
    rows = []
    rng = range(a, b+1) if a < b else range(b, a+1)
    for i in rng:
        qr = pyqrcode.create(i)
        fileName = qrPath + '{}.png'.format(i)
        qr.png(fileName, scale=5)
        rows.append([fileName, i])

    with open(qrPath + 'codes.csv', "w", newline="") as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerows(rows)
